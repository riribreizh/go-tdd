package racer

import (
	"fmt"
	"net/http"
	"time"
)

var tenSecondsTimeout = 10 * time.Second

func Racer(url1, url2 string) (string, error) {
	return ConfigurableRacer(url1, url2, tenSecondsTimeout)
}

func ConfigurableRacer(url1, url2 string, timeout time.Duration) (winner string, err error) {
	select {
	case <-ping(url1):
		return url1, nil
	case <-ping(url2):
		return url2, nil
	case <-time.After(timeout):
		return "", fmt.Errorf("timeout waiting server responses on %s and %s", url1, url2)
	}
}

type Empty struct{}

func ping(url string) chan Empty {
	ch := make(chan Empty)
	go func() {
		http.Get(url)
		close(ch)
	}()
	return ch
}

func RacerOld(url1, url2 string) (winner string) {
	duration1 := measureResponseTime(url1)
	duration2 := measureResponseTime(url2)

	if duration1 < duration2 {
		winner = url1
	} else {
		winner = url2
	}
	return
}

func measureResponseTime(url string) time.Duration {
	start := time.Now()
	http.Get(url)
	return time.Since(start)
}
