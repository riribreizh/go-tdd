package dictionary

const (
	ErrNotFound     = DictionaryErr("unknown key")
	ErrWordExists   = DictionaryErr("word already exists")
	ErrDoesNotExist = DictionaryErr("word does not exist")
)

type Dictionary map[string]string
type DictionaryErr string

func (e DictionaryErr) Error() string {
	return string(e)
}

func (d Dictionary) Search(word string) (string, error) {
	str, ok := d[word]
	if !ok {
		return "", ErrNotFound
	}
	return str, nil
}

func (d Dictionary) Add(word, value string) error {
	_, err := d.Search(word)
	switch err {
	case ErrNotFound:
		d[word] = value
	case nil:
		return ErrWordExists
	default:
		return err
	}
	return nil
}

func (d Dictionary) Update(word, value string) error {
	_, err := d.Search(word)
	if err == nil {
		d[word] = value
	} else if err == ErrNotFound {
		err = ErrDoesNotExist
	}
	return err
}

func (d Dictionary) Delete(word string) error {
	_, err := d.Search(word)
	if err == ErrNotFound {
		return ErrDoesNotExist
	}
	delete(d, word)
	return nil
}
