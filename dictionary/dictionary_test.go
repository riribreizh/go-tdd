// https://quii.gitbook.io/learn-go-with-tests/go-fundamentals/maps
package dictionary

import (
	"testing"
)

const (
	testedString  = "this is just a test"
	updatedString = "this is the update test"
)

func TestSearch(t *testing.T) {
	dict := Dictionary{"test": testedString}

	t.Run("known word", func(t *testing.T) {
		assertDefinition(t, dict, "test", testedString)
	})

	t.Run("unknown word", func(t *testing.T) {
		_, err := dict.Search("unknown")
		assertError(t, err, ErrNotFound)
	})
}

func TestAdd(t *testing.T) {
	t.Run("new word", func(t *testing.T) {
		dict := Dictionary{}
		err := dict.Add("test", testedString)
		assertError(t, err, nil)
		assertDefinition(t, dict, "test", testedString)
	})

	t.Run("existing word", func(t *testing.T) {
		dict := Dictionary{"test": testedString}
		err := dict.Add("test", "other string")
		assertError(t, err, ErrWordExists)
		assertDefinition(t, dict, "test", testedString)
	})
}

func TestUpdate(t *testing.T) {
	t.Run("update existing", func(t *testing.T) {
		dict := Dictionary{"test": testedString}
		err := dict.Update("test", updatedString)
		assertError(t, err, nil)
		assertDefinition(t, dict, "test", updatedString)
	})
	t.Run("update non existing", func(t *testing.T) {
		dict := Dictionary{}
		err := dict.Update("test", updatedString)
		assertError(t, err, ErrDoesNotExist)
	})
}

func TestDelete(t *testing.T) {
	t.Run("delete existing", func(t *testing.T) {
		dict := Dictionary{"test": testedString}
		err := dict.Delete("test")
		assertError(t, err, nil)
	})
	t.Run("delete non existing", func(t *testing.T) {
		dict := Dictionary{}
		err := dict.Delete("test")
		assertError(t, err, ErrDoesNotExist)
	})
}

func assertError(t *testing.T, got, want error) {
	t.Helper()
	if got != want {
		if want != nil {
			t.Fatal("expected to get an error")
		} else {
			t.Errorf("got %q want %q", got, want)
		}
	}
}

func assertDefinition(t *testing.T, d Dictionary, word, want string) {
	t.Helper()

	got, err := d.Search(word)
	if err != nil {
		t.Fatal("should find word: ", err)
	}
	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}
