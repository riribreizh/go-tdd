package sync

import "sync"

type Counter struct {
	mu    sync.Mutex
	value int
}

// NewCounter creates a concurrent safe counter
func NewCounter() *Counter {
	return &Counter{}
}

// Inc increments the counter by 1
func (c *Counter) Inc() {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.value++
}

// Value returns the currrent value of the counter
func (c *Counter) Value() int {
	return c.value
}
