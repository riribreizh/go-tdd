package wallet

import (
	"fmt"
	"errors"
)

type Bitcoin int

func (b Bitcoin) String() string {
	return fmt.Sprintf("%d BTC", b)
}

type Wallet struct {
	balance Bitcoin
}

func (w *Wallet) Deposit(amount Bitcoin) {
	//fmt.Printf("address of balance in Deposit is %v \n", &w.balance)
	w.balance = amount
}

func (w *Wallet) Balance() Bitcoin {
	return w.balance
}

var NotEnoughFounds = errors.New("not enough founds")

func (w *Wallet) Withdraw(amount Bitcoin) error {
	if amount > w.balance {
		return NotEnoughFounds
	}
	w.balance -= amount
	return nil
}