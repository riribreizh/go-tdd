package integers

// Add adds two integers and returns the sum of them.
func Add(x, y int) int {
	return x + y
}