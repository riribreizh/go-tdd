// https://quii.gitbook.io/learn-go-with-tests/go-fundamentals/reflection

package reflexion

import "testing"

import "reflect"

func TestWalk(t *testing.T) {

	cases := []struct {
		Name          string
		Input         interface{}
		ExpectedCalls []string
	}{
		{
			"Struct with one field",
			struct {
				Name string
			}{"Richard"},
			[]string{"Richard"},
		},
		{
			"Struct with two fields",
			struct {
				Name string
				City string
			}{"Richard", "Bordeaux"},
			[]string{"Richard", "Bordeaux"},
		},
		{
			"Struct with non string field",
			struct {
				Name string
				Age  int
			}{"Richard", 45},
			[]string{"Richard"},
		},
		{
			"Nested structs",
			Person{
				"Richard",
				Profile{45, "Bordeaux"},
			},
			[]string{"Richard", "Bordeaux"},
		},
		{
			"Pointers to things",
			&Person{
				"Richard",
				Profile{45, "Bordeaux"},
			},
			[]string{"Richard", "Bordeaux"},
		},
		{
			"Slices",
			[]Profile{
				{45, "Bordeaux"},
				{90, "Postapocaliptica"},
			},
			[]string{"Bordeaux", "Postapocaliptica"},
		},
		{
			"Arrays",
			[2]Profile{
				{45, "Bordeaux"},
				{90, "Postapocaliptica"},
			},
			[]string{"Bordeaux", "Postapocaliptica"},
		},
		{
			"Map",
			map[string]string{
				"Foo": "Bar",
				"Baz": "Boz",
			},
			[]string{"Bar", "Boz"},
		},
	}

	for _, test := range cases {
		t.Run(test.Name, func(t *testing.T) {
			var got []string
			walk(test.Input, func(input string) {
				got = append(got, input)
			})

			if !reflect.DeepEqual(got, test.ExpectedCalls) {
				t.Errorf("got %v, expected %v", got, test.ExpectedCalls)
			}
		})
	}

}

type Person struct {
	Name    string
	Profile Profile
}

type Profile struct {
	Age  int
	City string
}
